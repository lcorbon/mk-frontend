import { MutationFunction, QueryFunction } from "react-query";
import { API_HOST } from ".";
import getRandomUUID from "../uuid";

const ENDPOINT = API_HOST + "/creatives";

const DEFAULT_CREATIVE: Creative = {
  content: "",
  enabled: false,
  title: "",
  description: "",
  formats: [],
};

export function fetchCreatives(): Promise<Creative[]> {
  return fetch(ENDPOINT).then((res) => res.json());
}

export const getCreative: QueryFunction<Creative> = (queryParam) => {
  const { queryKey } = queryParam;
  const id = queryKey[1];
  if (!id) return DEFAULT_CREATIVE;
  const url = `${ENDPOINT}/${id}`;
  return fetch(url).then((res) => res.json());
};

export const createCreatives: MutationFunction<Creative, Creative> = (
  creative
) => {
  const finalCreative: Creative = {
    ...creative,
    id: getRandomUUID(),
    contributors: [],
    lastModified: new Date().toISOString(),
  };

  return fetch(ENDPOINT, {
    method: "POST",
    body: JSON.stringify(finalCreative),
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => res.json());
};
export const putCreatives: MutationFunction<Creative, Creative> = (
  creative
) => {
  const url = `${ENDPOINT}/${creative.id}`;

  return fetch(url, {
    method: "POST",
    body: JSON.stringify(creative),
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => res.json());
};

export function deleteCreatives(
  uuid: string | undefined
): MutationFunction<unknown, void> {
  const url = `${ENDPOINT}/${uuid}`;

  return () =>
    fetch(url, {
      method: "DELETE",
    }).then((res) => res.json());
}

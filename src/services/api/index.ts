import {QueryClient} from 'react-query'

export const API_HOST = "http://localhost:3001"

export const queryClient = new QueryClient()

import { API_HOST } from ".";

const ENDPOINT = API_HOST + "/user";

export function getUser() : Promise<User> {
  return fetch(ENDPOINT)
    .then((res) => res.json())
}


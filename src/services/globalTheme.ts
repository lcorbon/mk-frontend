import { createTheme } from "@mui/material";
import moduleName from '@mui/system/'


export const globalTheme = createTheme({
	palette: {
		primary: {
			main: '#444444'
		}
	}
})
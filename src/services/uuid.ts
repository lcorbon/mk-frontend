
// fix to support Crypto.randomUUID, will fail on Safari and IE
declare global {
  interface Crypto {
    randomUUID: () => string;
  }
}

export default function getRandomUUID() {
  return crypto.randomUUID();
}

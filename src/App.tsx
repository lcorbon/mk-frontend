import Container from "@mui/material/Container";
import ThemeProvider from "@mui/system/ThemeProvider";
import { QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter } from "react-router-dom";
import Router from "./app/Creative/Router";
import { queryClient } from "./services/api";
import { globalTheme } from "./services/globalTheme";

function App() {
  return (
    <ThemeProvider theme={globalTheme}>
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <Container sx={{ padding: "2em 0", height: "100%" }}>
            <Router />
          </Container>
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;

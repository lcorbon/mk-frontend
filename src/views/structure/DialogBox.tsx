import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { ReactNode } from "react";

interface Props {
  title: string;
  children: ReactNode;
  open: boolean;
  onClose: (event: {}) => void;
}

export function useDialogOpen(defaultOpen = false) {
  const [dialogOpen, setDialogOpen] = React.useState(defaultOpen);

  const openDialog = () => {
    setDialogOpen(true);
  };
  const closeDialog = () => {
    setDialogOpen(false);
  };

  const toggleDialog = () => {
    setDialogOpen(!dialogOpen);
  };

  return { dialogOpen, openDialog, closeDialog, toggleDialog };
}

const DialogBox = (props: Props) => {
  const { title, children, open, onClose } = props;

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
};

export default DialogBox;

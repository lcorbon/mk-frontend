import React from "react";
import Chip from "@mui/material/Chip";

interface Props {
  format: FormatDef;
  size?: "small"|"medium"
}

const FormatChip = (props: Props) => {
  const { format, size = "small" } = props;
  if (format)
    return <Chip size={size} label={`${format.width}x${format.height}`} />;
  console.warn("fail to render FormatChip of " + format);
  return <></>;
};

export default React.memo(FormatChip);

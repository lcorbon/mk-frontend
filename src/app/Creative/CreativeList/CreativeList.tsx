import React from "react";
import CreativeListItem from "./CreativeListItem";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TablePagination from "@mui/material/TablePagination";
import { useQuery } from "react-query";
import { fetchCreatives } from "../../../services/api/creativeQueries";
import useCreativeListState from "./useCreativeListState";

function CreativeList() {
  const creatives = useQuery("creatives", fetchCreatives).data || [];
  const {
    handleChangePage,
    handleChangeRowsPerPage,
    page,
    rowPerPage,
    slicedList,
  } = useCreativeListState(creatives);

  return (
    <div id="banner-list">
      <Table size="small">
        <TableBody>
          {Array.isArray(creatives) &&
            slicedList.map((creative) => (
              <CreativeListItem key={creative.id} creative={creative} />
            ))}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10]}
        component="div"
        count={creatives.length}
        rowsPerPage={rowPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <div>Preview</div>
    </div>
  );
}

export default CreativeList;

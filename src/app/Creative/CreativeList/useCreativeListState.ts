import React from "react";

const defaultRowPerPage = 5;

export default function useCreativeListState(creatives: Creative[]) {
  const [page, setPage] = React.useState(0);
  const [rowPerPage, setRowPerPage] = React.useState(defaultRowPerPage);

  const slicedList = creatives.slice(
    rowPerPage * page,
    rowPerPage * (page + 1)
  );

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return {
    handleChangePage,
    handleChangeRowsPerPage,
    slicedList,
    page,
    rowPerPage,
  };
}

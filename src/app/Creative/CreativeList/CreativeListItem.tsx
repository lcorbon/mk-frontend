import React from "react";
import Avatar from "@mui/material/Avatar";
import AvatarGroup from "@mui/material/AvatarGroup";
import Stack from "@mui/material/Stack";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Switch from "../../../views/inputs/Switch";
import FormatChip from "../../../views/display/FormatChip";
import { useNavigate } from "react-router";

type Props = {
  creative: Creative;
};

function getPersonInitial(user: User) {
  const { firstName, lastName } = user;

  const firstNameInitial = firstName.charAt(0).toLocaleUpperCase();
  const lastNameInitial = lastName.charAt(0).toLocaleUpperCase();

  return firstNameInitial + lastNameInitial;
}

function CreativeListItem(props: Props) {
  const { creative } = props;
  const navigate = useNavigate();

  function navigateToForm() {
    navigate("/creative/" + creative.id);
  }

  return (
    <TableRow>
      <TableCell onClick={navigateToForm}>{creative.title}</TableCell>
      <TableCell align="left">
        <AvatarGroup className="avatar-group" spacing="small">
          {Array.isArray(creative.contributors) &&
            creative.contributors.map((contributor) => {
              const initial = getPersonInitial(contributor);
              return <Avatar sizes="">{initial}</Avatar>;
            })}
        </AvatarGroup>
      </TableCell>
      <TableCell>
        <Stack direction="row" spacing={1}>
          {creative.formats.map((format) => (
            <FormatChip format={format} />
          ))}
        </Stack>
      </TableCell>
      <TableCell>
        <Switch checked={creative.enabled} />
      </TableCell>
    </TableRow>
  );
}

export default React.memo(CreativeListItem);

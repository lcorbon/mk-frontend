import React from "react";
import Stack from "@mui/material/Stack";
import FormatChip from "../../../views/display/FormatChip";
import Button from "@mui/material/Button";
import DialogActions from "@mui/material/DialogActions";
import TextField from "@mui/material/TextField";
import DialogBox, { useDialogOpen } from "../../../views/structure/DialogBox";
import { FormEvent, ChangeEvent } from "react";

const DEFAULT_FORMAT: FormatDef = {
  height: 200,
  width: 600,
};

interface Props {
  formats?: FormatDef[];
  onChange: (formats: FormatDef[]) => void;
}

const CreativeFormFormats = (props: Props) => {
  const { formats, onChange } = props;

  const { dialogOpen, openDialog, closeDialog } = useDialogOpen();

  const [newFormat, setNewFormat] = React.useState<FormatDef>(DEFAULT_FORMAT);

  const newFormatChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { id, value } = event.currentTarget;
    if (parseInt(value, 10) >= 0)
      setNewFormat({
        ...newFormat,
        [id]: value,
      });
  };

  const addFormat = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.stopPropagation();
    if (Array.isArray(formats)) {
      onChange([...formats, newFormat]);
    } else {
      onChange([newFormat]);
    }
  };

  return (
    <Stack
      direction="row"
      flexWrap="wrap"
      alignItems="center"
      rowGap={1}
      gap={1}
    >
      {Array.isArray(formats) &&
        formats.map((format, index) => (
          <FormatChip key={index} format={format} />
        ))}
      <Button
        sx={{ borderRadius: "2em" }}
        variant="contained"
        color="primary"
        size="small"
        onClick={openDialog}
      >
        +
      </Button>
      <DialogBox
        open={dialogOpen}
        onClose={closeDialog}
        title="Ajouter un format"
      >
        <form onSubmit={addFormat}>
          <Stack>
            <TextField
              label="largeur"
              autoFocus
              id="width"
              type="number"
              margin="dense"
              required
              value={newFormat?.width}
              onChange={newFormatChangeHandler}
              inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
            />
            <TextField
              label="hauteur"
              id="height"
              type="number"
              margin="dense"
              required
              value={newFormat?.height}
              onChange={newFormatChangeHandler}
              inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
            />
          </Stack>
          <DialogActions>
            <Button color="primary" variant="contained" type="submit">
              Ajouter
            </Button>
            <Button variant="outlined" onClick={closeDialog}>
              Retour
            </Button>
          </DialogActions>
        </form>
      </DialogBox>
    </Stack>
  );
};

export default CreativeFormFormats;

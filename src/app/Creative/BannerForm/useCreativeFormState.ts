import { useState, useEffect } from "react";
import { useMutation, useQuery } from "react-query";
import { useNavigate, useParams } from "react-router";
import { FormEvent } from "react-transition-group/node_modules/@types/react";
import {
  createCreatives,
  deleteCreatives,
  getCreative,
  putCreatives,
} from "../../../services/api/creativeQueries";
import { getUser } from "../../../services/api/userQueries";

const DEFAULT_CREATIVE: Creative = {
  content: "",
  enabled: false,
  title: "",
  description: "",
  formats: [],
};

const errorMessages = {
  required: "Ce champs est obligatoire",
  atLeastOneFormat: "Il doit y avoir au moins un format défini",
};

export default function validateCreativeForm(formState: Creative) {
  const errors = new Map<keyof Creative, string>();

  if (!formState.title) errors.set("title", errorMessages.required);
  if (!formState.description) errors.set("description", errorMessages.required);
  if (!formState.content) errors.set("content", errorMessages.required);
  if (!formState.formats || formState.formats.length <= 0)
    errors.set("formats", errorMessages.atLeastOneFormat);

  return errors;
}

export function useCreativeFormState(
  initialCreative: Creative = DEFAULT_CREATIVE
) {
  const navigate = useNavigate();
  const urlParam = useParams();
  const [formState, setFormState] = useState(initialCreative);
  const [formErrors, setFormErrors] = useState(new Map<string, string>());
  const user = useQuery("user", getUser).data;
  const creativeQuery = useQuery(["creative", urlParam.id], getCreative);
  const createMutation = useMutation(createCreatives);
  const putMutation = useMutation(putCreatives);
  const deleteMutation = useMutation(deleteCreatives(urlParam.id));
  const save = urlParam.id ? putMutation : createMutation;

  useEffect(() => {
    if (!creativeQuery.isLoading) {
      if (creativeQuery.data) {
        console.log(`data`, creativeQuery.data);
        setFormState(creativeQuery.data);
      }
    }
  }, [creativeQuery.isLoading]);

  const enabledHandler: React.ChangeEventHandler<HTMLElement> = () => {
    setFormState({
      ...formState,
      enabled: !formState.enabled,
    });
  };

  const changeHandler = (key: string, value: string | FormatDef[]) => {
    formErrors.delete(key);
    setFormState({
      ...formState,
      [key]: value,
    });
  };

  const textFieldChangeHandler: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const { value, id } = event.target;
    changeHandler(id, value);
  };

  const formatsChangeHandler = (formats: FormatDef[]) => {
    changeHandler("formats", formats);
  };

  const closeHandler = () => {
    navigate("/creatives");
  };

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const errors = validateCreativeForm(formState);
    if (errors.size > 0) setFormErrors(errors);
    else {
      const creative: Creative = {
        ...formState,
        createdBy: user,
      };
      save.mutateAsync(creative).then(() => {
        closeHandler();
      });
    }
  };

  const deleteHandler = () => {
    deleteMutation &&
      deleteMutation.mutateAsync().then(() => {
        closeHandler();
      });
  };

  return {
    formState,
    formErrors,
    enabledHandler,
    textFieldChangeHandler,
    formatsChangeHandler,
    submitHandler,
    deleteHandler,
    closeHandler,
  };
}

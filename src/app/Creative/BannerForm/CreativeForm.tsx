import React, { ReactElement } from "react";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { styled } from "@mui/system";
import Switch from "../../../views/inputs/Switch";
import BannerFormFormats from "./CreativeFormFormats";
import { useCreativeFormState } from "./useCreativeFormState";

interface Props {
  creative?: Creative;
  className?: string;
}

function CreativeForm(props: Props): ReactElement {
  const { creative, className } = props;

  const {
    formState,
    formErrors,
    enabledHandler,
    textFieldChangeHandler,
    formatsChangeHandler,
    submitHandler,
    deleteHandler,
    closeHandler,
  } = useCreativeFormState(creative);

  return (
    <form className={className} onSubmit={submitHandler}>
      <Stack sx={{ maxWidth: 400, width: "100%" }} gap={2}>
        <Stack direction="row" justifyContent="space-between">
          <TextField
            id="title"
            label="Titre"
            value={formState?.title}
            size="small"
            error={formErrors.has("title")}
            helperText={formErrors.get("title")}
            onChange={textFieldChangeHandler}
          />
          <Stack direction="row" alignItems="center">
            <Typography>Activée</Typography>
            <Switch checked={formState?.enabled} onChange={enabledHandler} />
          </Stack>
        </Stack>
        <TextField
          id="description"
          label="Description"
          multiline
          rows={2}
          value={formState?.description}
          size="small"
          error={formErrors.has("description")}
          helperText={formErrors.get("description")}
          onChange={textFieldChangeHandler}
        />
        <TextField
          id="content"
          label="Contenu"
          multiline
          rows={4}
          value={formState?.content}
          size="small"
          error={formErrors.has("content")}
          helperText={formErrors.get("content")}
          onChange={textFieldChangeHandler}
        />
        <BannerFormFormats
          formats={formState?.formats}
          onChange={formatsChangeHandler}
        />
        {formErrors.has("formats") && (
          <Typography variant="caption" color="error">
            {formErrors.get("formats")}
          </Typography>
        )}
      </Stack>
      <Stack
        direction="row"
        justifyContent="center"
        gap={2}
        sx={{ marginTop: 2 }}
      >
        <Button color="primary" variant="contained" type="submit">
          Enregistrer
        </Button>
        <Button variant="outlined" onClick={closeHandler}>
          Annuler
        </Button>
        <Button
          color="error"
          variant="outlined"
          onClick={deleteHandler}
          disabled={!!creative?.id}
        >
          Supprimer
        </Button>
      </Stack>
    </form>
  );
}

const StyledCreativeForm = styled(CreativeForm)`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
`;

export default StyledCreativeForm;

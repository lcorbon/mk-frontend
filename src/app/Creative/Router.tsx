import React from "react";
import { Route, Routes } from "react-router-dom";
import CreativeForm from "./BannerForm/CreativeForm";
import CreativeList from "./CreativeList/CreativeList";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<CreativeList />} />
      <Route path="/creatives" element={<CreativeList />} />
      <Route path="/creative/:id" element={<CreativeForm />} />
      <Route path="/creative" element={<CreativeForm />} />
    </Routes>
  );
};

export default Router;

interface FormatDef {
  width: number;
  height: number;
}

declare type User = {
  id?: string;
  firstName: string;
  lastName: string;
};

declare type Creative = {
  id?: string;
  createdBy?: User;
  contributors?: User[];
  lastModified?: string;
  enabled: boolean;
  title: string;
  description: string;
  content: string;
  formats: FormatDef[];
};
